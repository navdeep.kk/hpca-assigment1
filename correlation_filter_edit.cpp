#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    int *copy = new int[height * width];

    for(int i = 0; i < filters; i++){

        for(int h =0; h < height; ++h){
        
            for(int w = 0; w < width; ++w){
                
                if(i == 0){
                copy[h * width + w] = 0;
                for(int offx = -1; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 1; ++offy)
                    {
                        if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                            continue;
                        copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                            filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
                }
                else if (i == 1){
                    image[h * width + w] = 0;
                    for(int offx = -1; offx <= 1; ++offx){
                        for(int offy = -1; offy <= 1; ++offy)
                        {
                            if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                                continue;
                            image [h * width + w] += copy[(offx + h) * width + offy + w] * 
                                filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                        }   
                    }
                }
            }
        }
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
